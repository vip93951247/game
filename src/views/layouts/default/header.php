<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="title" content="АМОД">
    <meta name="description" content="АМОД - тест по выявлению стресса.">
    <meta name="author" content="Miroslaw">

    <title>АМОД - тест по выявлению стресса</title>

    <script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
    <link rel="stylesheet" href="<?php echo '/public/css/styles.css'?>" >
    <script src="<?php echo '/public/js/main.js'?>" ></script>
    <script src="<?php echo '/public/js/moment.min.js'?>" ></script>
</head>

<body onload="App.observer()">

<div id="wrapper">